---
title: "about"
date: 2020-10-20T17:51:47+03:30
draft: false
headless: true

full_name: "HENRIQUE DONÂNCIO"
profile_picture: "profile.png"
cv: "cv.pdf"
# set to false if you don't want to show your blog
blog: false

socials:
    gitlab: "hdonancio"
    email: "mailto:henrique.donancio@inria.fr"
    
interests:
    - Reinforcement Learning
    - Machine Learning
    - Multi-agent Systems
    
affiliations:
    - affiliation:
        title: "Postdoc"
        name: "INRIA Grenoble"
        other_info: "From Setember 2023"
    
academia:
    - course:
        degree: "Ph.D."
        institution:  "INSA Rouen Normandie"
        major: "Artificial Intelligence"
        start_date: "2019"
        other_info: 'supervised by Prof. Laurent Vercouter'
    - course:
        degree: "M.Sc."
        institution: 'University of São Paulo (USP)'
        major: 'Computer Science'
        start_date: '2017'
        end_date: '2019'
        other_info: 'supervised by Prof. Anarosa A. F. Brandão'
    - course:
        degree: "B.Sc."
        institution: 'Federal University of Rio Grande (FURG)'
        major: 'Computer Engineering'
        start_date: '2011'
        end_date: '2017'
        other_info: 'supervised by Prof.  Graçaliz Dimuro and Prof. Diana Adamatti'
---
I am currently a Postdoc Researcher at [Statify][4] team of Inria Grenoble, and my research is related to optimization using Deep Reinforcement Learning to enhance Magnetic Reasonance Images (MRI) acquisitions. This work is a partnership between Grenoble Institut des Neurosciences (GIN) lead by [Thomas Christen][5] and INRIA's Statify team lead by [Florence Forbes][6]. Before I did my Ph.D. at [LITIS lab][1] as part of the [IoT.H2O project][2] under Prof. [Laurent Vercouter's][3] supervision. My research where focused on enhancing the learning process by using logged data or structuring it through curricula.

Among the research topics I am interested includes exploiting knowledge from static datasets (learning from demonstrations), transfer learning, distributional and applied RL. 

[1]: https://www.litislab.fr/
[2]: https://www.mv.uni-kl.de/IoTDotH2O/en/home-2/
[3]: https://pagesperso.litislab.fr/lvercouter/
[4]: https://team.inria.fr/statify/
[5]: https://neurosciences.univ-grenoble-alpes.fr/fr/annuaire/thomas-christen-743104.kjsp
[6]: http://mistis.inrialpes.fr/people/forbes/
